<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Company;
use App\Model\Employee;
use Image;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companys = Company::paginate(10);
        return view('admin.company.index',compact('companys'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name'=>'required',
            'logo'=>'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:max_width=100,max_height=100',
             'website'=>'required',
             'email' => 'required|email|max:100|unique:companys',
          
        ]);


         $company = new Company();


        if ($request->hasFile('logo')) {
            $image      = $request->file('logo');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();                 
            });

            $img->stream(); 

            Storage::disk('local')->put('public/images'.'/'.$fileName, $img, 'public');
        
            }

// dd($fileName);
        $company->logo = $fileName;
        $company->name = $request->name;
        $company->website = $request->website;
        // $company->logo = $request->logo;
        $company->email = $request->email;


        $company->save();

       return redirect()->route('company.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);
        // dd($company);
        return view('admin.company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'name'=>'required',
            'logo'=>'sometimes|image|mimes:jpeg,png,jpg,gif,svg|dimensions:max_width=100,max_height=100',
             'website'=>'required',
             'email' => 'required|email|max:100|unique:companys,email,'.$id,
            
          
        ]);


         $company = Company::findOrFail($id);

        if ($request->hasFile('logo')) {
            $image      = $request->file('logo');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();                 
            });

            $img->stream(); 

            Storage::disk('local')->put('public/images'.'/'.$fileName, $img, 'public');
            $company->logo = $fileName;
            
             if(\Storage::exists('public/images/'.$company->logo)){
            // dd('file exists');

                \Storage::delete('public/images/'.$company->logo);
                //Storage::delete($path . $filename_old);

              }

            }

                
                $company->name = $request->name;
                $company->website = $request->website;
                // $company->logo = $request->logo;
                $company->email = $request->email;


                $company->save();

               return redirect()->route('company.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $company = Company::findorFail($id);
            $company->delete();
             if(\Storage::exists('public/images/'.$company->logo)){
                // dd('file exists');

                    \Storage::delete('public/images/'.$company->logo);
                //Storage::delete($path . $filename_old);

              }
        return redirect()->route('company.index');
    }

    public function assingEmployee(Request $request){
        // dd($request->all());

        if ($request->has('comp_id')) {
            // dd('success');
          $emp_id = $request->emp_id;
        $comp_id = $request->comp_id;

        $employee = Employee::findOrFail($emp_id);
        $employee->company_id = $comp_id;
        $employee->save();




            $response['status'] = "success";
            $response['company'] = $comp_id;
            $response['employee'] = $employee;
            return response()->json($response);
        }
        

    }
}
