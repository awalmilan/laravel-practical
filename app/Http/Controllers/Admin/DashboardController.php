<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Employee;
use App\Model\Company;

class DashboardController extends Controller
{
    public function index(){
    	$employee = Employee::whereHas('companies',function($query){
    		$query->where('id','>',0);
    	},'=',0)->get();

 			// dd($employee);



    	$companies = Company::with('employees')->get();
    	// dd($companies);
    	return view('admin.index',compact('employee','companies'));
    }
}
