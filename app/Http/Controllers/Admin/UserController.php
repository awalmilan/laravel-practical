<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(){
    	$users = User::all();
    	// dd($users);
    	return view('admin.users.index',compact('users'));
    }

     public function create(){
    	
    	// dd($users);
    	return view('admin.users.create');
    }

 

    public function store(Request $request){
    	// dd($request->all());
    	$this->validate($request,[
    		'firstname'=>'required',
    		'lastname' => 'required',
    		'email' => 'required|email|max:100|unique:users',
    		'password' => 'required|min:4|confirmed',
    	]);


    	$user = new User();
    	$user->firstname = $request->firstname;
    	$user->lastname  = $request->lastname;
    	$user->address = $request->address;
    	$user->email = $request->email;
    	$user->password = bcrypt($request->password);
    	$user->save();
    	return redirect()->route('users');
    }

    public function edit($id){
    	$user = User::findorFail($id);
    	return view('admin.users.edit',compact('user'));
    }

    public function update(Request $request, $id){
    	// dd('test');
    	  $this->validate($request,[
            'firstname'=>'required|max:50',
            'lastname'=>'required|max:50',
           
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'sometimes|nullable|string|min:4|max:10',  
        ]);

    	 $user = User::findOrFail($id);

        	$user->firstname = $request->input('firstname');
	        $user->lastname = $request->input('lastname');
	        $user->email = $request->input('email');
	      
	        $user->address = $request->input('address');
	     
	        if(!empty($request->get('password'))){
	            $user->password = bcrypt($request->input('password'));
	        }
	        $user->save();
        
        return redirect()->route('users');
    }

     public function logOut(){
        Auth::logout();
        return redirect()->route('login');
   
    }

    public function delete($id){
    	$user = User::findorFail($id);
    	$user->delete();
    	return redirect()->route('users');
    }
}
