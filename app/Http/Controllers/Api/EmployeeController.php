<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Employee;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $this->validate($request, [
            'phone' => 'required|min:10|max:10|unique:employee',
            'email' => 'required|email|unique:employee',
            'firstname' => 'required',
            'lastname' => 'required',
           
        ]);

         $employee = new Employee();
         $employee->phone = $request->phone;
         $employee->email = $request->email;
         $employee->firstname = $request->firstname;
         $employee->lastname = $request->lastname;
         $res = $employee->save();

         if ($res) {
             return response()->json(array(
                'error' => false,
                'message' => 'Empmloyee successfully created',
                'data' => $employee,
             ));
         }else{
            return response()->json(array(
                'error' => true,
                'message' => 'Empmloyee can not be created',
            ));
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'phone' => 'sometimes|min:10|max:10|unique:employee,phone,'.$id,
            'email' => 'required|email|unique:employee,email,'.$id,
            'firstname' => 'required',
            'lastname' => 'required',
           
        ]);

         $employee = Employee::findOrFail($id);

          if ($request->input('firstname')) {
                    $employee->firstname = $request->input('firstname');
                }
                if ($request->input('lastname')) {
                    $employee->lastname = $request->input('lastname');
                }
                if ($request->input('email')) {
                    $employee->email = $request->input('email');
                }
                if ($request->input('phone')) {
                    $employee->phone = $request->input('phone');
                }


      
         $res = $employee->save();

         if ($res) {
             return response()->json(array(
                'error' => false,
                'message' => 'Empmloyee successfully updated',
                'data' => $employee,
             ));
         }else{
            return response()->json(array(
                'error' => true,
                'message' => 'Empmloyee can not be updated',
            ));
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $company = Employee::findorFail($id);
            $res = $company->delete();

             if ($res) {
             return response()->json(array(
                'error' => false,
                'message' => 'Empmloyee successfully deleted',
             ));
         }else{
            return response()->json(array(
                'error' => true,
                'message' => 'Empmloyee can not be deleted',
            ));
         }
            
    }
}
