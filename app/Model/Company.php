<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Employee;

class Company extends Model
{

	 protected $table = 'companys';
	
    protected $fillable = [
        'name','logo' ,'status','email', 'webiste',
    ];

    public function employees(){
    	return $this->hasMany(Employee::class);
    }
}
