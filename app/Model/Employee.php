<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Company;

class Employee extends Model
{
	protected $table = 'employee';
    	
    protected $fillable = [
        'firstname','lastname' ,'status','email', 'phone','company_id',
    ];


    public function companies(){
        return $this->belongsTo(Company::class, 'company_id');
    }
}
