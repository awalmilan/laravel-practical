<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Model\Employee;

class PhoneValidation implements Rule
{

    protected $phone
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($phone)
    {
        $this->phone = $phone
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $condition = [
            'phone' = $this->phone
        ];
        $phone  = Employee::where($condition)->where('phone','!=',$this->phone)->first();
        if (isset($phone)) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Data already exists';
    }
}
