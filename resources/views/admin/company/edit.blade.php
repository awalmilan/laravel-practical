@extends('layouts.master')
@section('page_title')
| Edit Company
@endsection
@section('contents')
<div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
             
              <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12 mdc-layout-grid__cell--span-8-tablet">

                <div class="mdc-card">
                  <div class="template-demo">
                    @if ($errors->any())
    <div class="alert alert-warning">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                  <h6 class="card-title card-padding pb-0">Update Company</h6>

              <form action="{{ route('company.update',$company->id) }}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                    <div class="mdc-layout-grid">
                      <div class="mdc-layout-grid__inner">
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6-desktop">
                          <div class="mdc-text-field w-100">
                            <input type="text" name="name" value="{{ $company->name }}" class="mdc-text-field__input" id="text-field-hero-input">
                            <div class="mdc-line-ripple"></div>
                            <label for="text-field-hero-input" class="mdc-floating-label">Name <span style="color: #ff0000;padding-left: 2px;">*</span></label>
                          </div>
                        </div>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6-desktop">

                          <div class="mdc-text-field w-100">
                            <img src="{{ asset('storage/images/'.$company->logo) }}" width="50px"
                                                height="50px"/>
                           
                      <input type="file" name="logo" class="form-control" id="banner" value="{{ $company->logo }}" placeholder="Logo" value="{{ old('banner') }}">
                      <label for="exampleInputEmail3">Logo <span style="color: #ff0000;padding-left: 0px;">*</span></label>
                            
                          
                          </div>
                        </div>

                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6-desktop">
                          <div class="mdc-text-field w-100">
                            <input type="text" name="website" value="{{ $company->website }}" class="mdc-text-field__input" id="text-field-hero-input">
                            <div class="mdc-line-ripple"></div>
                            <label for="text-field-hero-input" class="mdc-floating-label">Website</label>
                          </div>
                        </div>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                          <div class="mdc-text-field w-100">
                            <input type="email" name="email" value="{{ $company->email }}" class="mdc-text-field__input" id="text-field-hero-input">
                            <div class="mdc-line-ripple"></div>
                            <label for="text-field-hero-input" class="mdc-floating-label">Email <span style="color: #ff0000;padding-left: 2px;">*</span></label>
                          </div>
                        </div>
                      
                       
                        
                       
                       
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                          <button type="submit" class="mdc-button mdc-button--raised">
                           Update Company
                        </button>
                        </div>
                      </div>
                    </div>
                  </form>

                </div>
                </div>
              </div>
           
            </div>
          </div>
@endsection