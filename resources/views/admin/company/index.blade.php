@extends('layouts.master')
@section('page_title')
| Companies
@endsection
@section('styles')
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
 

@endsection
@section('contents')
<div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
             
              <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12 mdc-layout-grid__cell--span-8-tablet">

                <div class="mdc-card">
                  <div class="template-demo">
                   
                  <a href="{{ route('company.create') }}"><button type="button" class="mdc-button mdc-button--raised">
                      Create Company
                    </button></a>
                  <h6 class="card-title card-padding pb-0">Company Table</h6>

                  <div class="table-responsive">

                    <table class="table table-hoverable">
                      <thead>
                        <tr>
                          <th class="text-left">Name</th>
                          <th class="text-left">Last Name</th>
                          <th class="text-left">Email</th>
                          <th class="text-left">Status</th>
                          <th class="text-left">Action</th>
                        
                          
                         
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($companys as $company)
                        <tr >
                          <td class="text-left">{{ $company->name }}</td>
                          <td class="text-left">{{ $company->logo }}
                            <img src="{{ asset('storage/images/'.$company->logo) }}" width="50px"
                                                height="50px"/></td>
                          <td class="text-left">{{ $company->email }}</td>
                          <td class="text-left">{{ $company->website }}</td>
                         <td class="text-left">
                             <a href="{{ route('company.edit',$company->id) }}" class="mdc-button mdc-button--raised">
                                    edit
                                </a>
                            
                                <form action="{{ route('company.destroy',$company->id) }}" class="delform"
                                      style="display: inline;" method="post"
                                      onsubmit="return confirm('Are you sure')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="mdc-button mdc-button--raised icon-button filled-button--secondary">
                        <i class="material-icons mdc-button__icon">delete</i>
                      </button>
                                </form>
                         </td>
                        </tr>
                        @endforeach
                       
                      </tbody>
                      {{ $companys->links() }}
                    </table>
                  </div>


                </div>
                </div>
              </div>
           
            </div>
          </div>
@endsection