@extends('layouts.master')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('page_title')
| Home
@endsection
@section('styles')
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <style>
    #draggable { 
        width: 150px;
        height: 150px;
        padding: 0.5em;
    }
  </style>
@endsection
@section('contents')
 
 <div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
              <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-desktop">
                <div class="mdc-card">
                  <h6 class="card-title">Employess</h6>
                  
                   <ul class="list-group shadow-lg connectedSortable" id="padding-item-drop">
                  @foreach($employee as $emp)
                      <li class="list-group-item" item-id="{{ $emp->id }}">{{ $emp->firstname }}</li>
                  @endforeach
                </ul>

             
                </div>
              </div>
              @foreach($companies as $cmp)
              <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-desktop">
                <div class="mdc-card">
                  <h6 class="card-title">{{ $cmp->name }}</h6>
                   <ul class="list-group  connectedSortable" id="complete-item-drop" item-id="{{ $cmp->id }}">
                  @foreach($cmp->employees as $emp)
                      <li class="list-group-item " item-id="{{ $emp->id }}">{{ $emp->firstname }}</li>

                   @endforeach
                   <li class="list-group-item " item-id=""></li>
                </ul>
                </div>
              </div>
              @endforeach
              
            </div>
          </div>
@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    var comp_id;
    $( "#padding-item-drop, #complete-item-drop" ).sortable({
      connectWith: ".connectedSortable",
      opacity: 0.5,
       receive: function(event, ui) {
    comp_id = event.target.getAttribute("item-id");
    
    // do something with the group
}
    }).disableSelection();

    $( ".connectedSortable" ).on( "sortupdate", function( event, ui) {

        var emp_id = ui.item.attr("item-id");
        var cmp_id = comp_id;

        // test = $('.connectedSortable').attr('item-id');

         
        
        // console.log(emp_id,cmp_id);

        $.ajax({
            url: "{{ route('employee.assign') }}",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {emp_id:emp_id,comp_id:comp_id},
            success: function(data) {
              console.log('success');
            }
        });
          
    });
  });
</script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
</script>

@endsection