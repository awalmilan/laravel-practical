@extends('layouts.master')
@section('page_title')
| Users
@endsection
@section('contents')
<div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
             
              <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12 mdc-layout-grid__cell--span-8-tablet">

                <div class="mdc-card">
                  <div class="template-demo">
                    @if ($errors->any())
    <div class="alert alert-warning">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                  <h6 class="card-title card-padding pb-0">Create User</h6>

              <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
                @method('POST')
                @csrf
                    <div class="mdc-layout-grid">
                      <div class="mdc-layout-grid__inner">
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6-desktop">
                          <div class="mdc-text-field w-100">
                            <input type="text" name="firstname" class="mdc-text-field__input" id="text-field-hero-input">
                            <div class="mdc-line-ripple"></div>
                            <label for="text-field-hero-input" class="mdc-floating-label">Firstname <span style="color: #ff0000;padding-left: 2px;">*</span></label>
                          </div>
                        </div>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6-desktop">
                          <div class="mdc-text-field w-100">
                            <input type="text" name="lastname" class="mdc-text-field__input" id="text-field-hero-input">
                            <div class="mdc-line-ripple"></div>
                            <label for="text-field-hero-input" class="mdc-floating-label">Lastname <span style="color: #ff0000;padding-left: 2px;">*</span></label>
                          </div>
                        </div>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6-desktop">
                          <div class="mdc-text-field w-100">
                            <input type="text" name="address" class="mdc-text-field__input" id="text-field-hero-input">
                            <div class="mdc-line-ripple"></div>
                            <label for="text-field-hero-input" class="mdc-floating-label">Address</label>
                          </div>
                        </div>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                          <div class="mdc-text-field w-100">
                            <input type="email" name="email" class="mdc-text-field__input" id="text-field-hero-input">
                            <div class="mdc-line-ripple"></div>
                            <label for="text-field-hero-input" class="mdc-floating-label">Email <span style="color: #ff0000;padding-left: 2px;">*</span></label>
                          </div>
                        </div>
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                          <div class="mdc-text-field w-100">
                            <input class="mdc-text-field__input" type="password" name="password" id="text-field-hero-input">
                            <div class="mdc-line-ripple"></div>
                            <label for="text-field-hero-input" class="mdc-floating-label">Password <span style="color: #ff0000;padding-left: 2px;">*</span></label>
                          </div>
                        </div>
                       
                         <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                          <div class="mdc-text-field w-100">
                            <input class="mdc-text-field__input" type="password" name="password_confirmation" id="text-field-hero-input">
                            <div class="mdc-line-ripple"></div>
                            <label for="text-field-hero-input" class="mdc-floating-label">Confirm Password <span style="color: #ff0000;padding-left: 2px;">*</span></label>
                          </div>
                        </div>
                       
                       
                        <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-6">
                          <button type="submit" class="mdc-button mdc-button--raised">
                           Create User
                        </button>
                        </div>
                      </div>
                    </div>
                  </form>

                </div>
                </div>
              </div>
           
            </div>
          </div>
@endsection