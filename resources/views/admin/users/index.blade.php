@extends('layouts.master')
@section('page_title')
| Users
@endsection
@section('contents')
<div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
             
              <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12 mdc-layout-grid__cell--span-8-tablet">

                <div class="mdc-card">
                  <div class="template-demo">
                   
                  <a href="{{ route('user.create') }}"><button type="button" class="mdc-button mdc-button--raised">
                      Create User
                    </button></a>
                  <h6 class="card-title card-padding pb-0">User Table</h6>

                  <div class="table-responsive">

                    <table class="table table-hoverable">
                      <thead>
                        <tr>
                          <th class="text-left">Name</th>
                          <th class="text-left">Last Name</th>
                          <th class="text-left">Email</th>
                          <th class="text-left">Status</th>
                          <th class="text-left">Action</th>
                        
                          
                         
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($users as $user)
                        <tr >
                          <td class="text-left">{{ $user->firstname }}</td>
                          <td class="text-left">{{ $user->lastname }}</td>
                          <td class="text-left">{{ $user->email }}</td>
                          <td class="text-left">{{ $user->status == 1?'Active':'Inactive' }}</td>
                         <td class="text-left">
                             <a href="{{ route('user.edit',$user->id) }}" class="mdc-button mdc-button--raised">
                                    edit
                                </a>
                            
                                <form action="{{ route('user.delete',$user->id) }}" class="delform"
                                      style="display: inline;" method="post"
                                      onsubmit="return confirm('Are you sure')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="mdc-button mdc-button--raised icon-button filled-button--secondary">
                        <i class="material-icons mdc-button__icon">delete</i>
                      </button>
                                </form>
                         </td>
                        </tr>
                        @endforeach
                       
                      </tbody>
                    </table>
                  </div>


                </div>
                </div>
              </div>
           
            </div>
          </div>
@endsection