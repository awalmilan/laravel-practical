<!DOCTYPE html>
<html lang="en">
<head>
 <!-- header -->
 @include('layouts.header')
 @yield('styles')


</head>
<body>
<script src="{{ asset('admin/assets/js/preloader.js') }}"></script>
  <div class="body-wrapper">
    <!-- partial:../../partials/_sidebar.html -->
    <aside class="mdc-drawer mdc-drawer--dismissible mdc-drawer--open">
     <!-- side nav bar -->
     @include('layouts.sidenav')
    </aside>
    <!-- partial -->
    <div class="main-wrapper mdc-drawer-app-content">
      <!-- partial:../../partials/_navbar.html -->
      <header class="mdc-top-app-bar">
       <!-- top menu bar -->
       @include('layouts.menu')
      </header>
      <!-- partial -->
      <div class="page-wrapper mdc-toolbar-fixed-adjust">
        <main class="content-wrapper">
          @yield('contents')
        </main>
        <!-- partial:../../partials/_footer.html -->
        

       <!--  //footer -->

       @include('layouts.footer')
       @yield('script')
</body>
</html>