<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('api')->post('/login','Api\AuthController@login');

Route::group(['middleware'=>['auth:api']],function(){
	Route::post('employee/store','Api\EmployeeController@store')->name('employee.store');
	Route::post('employee/update/{id}','Api\EmployeeController@update')->name('employee.update');
	Route::delete('employee/delete/{id}','Api\EmployeeController@destroy');
});
