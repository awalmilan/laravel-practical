<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::group(['middleware'=>'auth'],function(){
Route::get('/', 'Admin\DashboardController@index')->name('home');
});
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
Route::get('/', 'Admin\DashboardController@index')->name('home');
Route::get('/users', 'Admin\UserController@index')->name('users');
Route::get('users/create', 'Admin\UserController@create')->name('user.create');

Route::post('users/store', 'Admin\UserController@store')->name('user.store');
Route::delete('users/delete/{id}', 'Admin\UserController@delete')->name('user.delete');

Route::get('users/edit/{id}','Admin\UserController@edit')->name('user.edit');
// Route::put('users/{$id}',function(){
// 	dd('sdfsd');
// })->name('user_update');

Route::put('users/update/{id}','Admin\UserController@update')->name('user_update');

Route::resource('company', 'Admin\CompanyController');

Route::post('employee/assign','Admin\CompanyController@assingEmployee')->name('employee.assign');

});

Route::get('logout', 'Admin\UserController@logOut')->name('adminlogout');
// Route::get('/home', 'HomeController@index')->name('home');
