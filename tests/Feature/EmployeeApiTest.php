<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;
use App\Model\Employee;

class EmployeeApiTest extends TestCase
{

    use RefreshDatabase;
    protected $user;

    public function loggedIn():void
    {
        parent::loggedIn();
          $$this->user = User::create(array(
                    'firstname' => 'admin',
                    'lastname' => 'las lan',
                    'password' => bcrypt('password'),
                    'email'    => 'admin@admin.com'
                ));
        $this->actingAs($$this->user,'api');

    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
     public function test_can_create_employees()
    {   

       
        $formData = [
            'firstname' => 'first employee',
            'lastname' => 'last name',
            'email' => 'abd@or.com',
            'phone' => '9820145214'
        ];
        
        $this->post(route('employee.store'),$formData)
                    ->assertStatus(200)
                    ->assertJson(['data'=>$formData])
                    ;
    }

     public function test_can_update_employees()
    {   

        $employee = factory(Employee::class)->make();
        $this->user->employees()->save($employee);

        $updatedData = [
            'firstname' => 'first employee',
            'lastname' => 'last name',
            'email' => 'abd@or.com',
            'phone' => '9820145214'
        ];
        $this->withoutExceptionHandling();
        $this->put(route('employee.update',$employee->id),$updatedData)
                    ->assertStatus(200)
                    ->assertJson(['data'=>$updatedData])
                    ;
    }
}
